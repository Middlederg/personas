﻿namespace Personas.Data.Enums
{
    public enum Comunidad
    {
        Andalucia = 1,
        Aragon = 2,
        Asturias = 3,
        Balears = 4,
        Canarias = 5,
        Cantabria = 6,
        CastillaLeon = 7,
        CastillaLaMancha = 8,
        Catalunya = 9,
        ComunidadValenciana = 10,
        Extremadura = 11,
        Galicia = 12,
        Madrid = 13,
        Murcia = 14,
        Navarra = 15,
        PaisVasco = 16,
        LaRioja = 17,
        Ceuta = 18,
        Melilla = 19,
    }
}
