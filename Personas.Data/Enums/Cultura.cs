﻿using System.ComponentModel.DataAnnotations;

namespace Personas.Data.Enums
{
    public enum Cultura
    {
        [Display(Name ="Española")]
        Espanola = 1,

        [Display(Name = "Inglesa")]
        Inglesa = 2,
    }
}
